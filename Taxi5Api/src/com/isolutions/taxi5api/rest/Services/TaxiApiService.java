package com.isolutions.taxi5api.rest.Services;

import com.isolutions.taxi5api.Models.Feedback;
import com.isolutions.taxi5api.Models.Location;
import com.isolutions.taxi5api.Models.Options;
import com.isolutions.taxi5api.Models.Order;
import retrofit.Callback;
import retrofit.http.*;

import java.util.ArrayList;

/**
 * Created by George Alekhovitch on 10.01.2015.
 */
public interface TaxiApiService {
    @GET("/api/locator/search")
    public ArrayList<Location> getLocations(@Query("q") String destination);

    @POST("/api/order")
    public Order makeOrder(@Body Order order);

    @POST("/api/order/{id}/cancel")
    public Order cancel(@Path("id") Integer id);

    @POST("/api/order/{id}/approve")
    public Order approveOrder(@Path("id") Integer id, @Body Options options);

    @GET("/api/order/{id}")
    public Order getOrderStatus(@Path("id") Integer id);

    @POST("/feedback")
    public void leaveFeedback(@Query("name") String name, @Query("phone") String phone, @Query("text") String text, Callback<Feedback> cb);
}
