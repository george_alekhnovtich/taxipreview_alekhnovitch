package com.isolutions.taxi5api.rest;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import com.isolutions.taxi5api.Models.Feedback;
import com.isolutions.taxi5api.Models.Location;
import com.isolutions.taxi5api.Models.Options;
import com.isolutions.taxi5api.Models.Order;
import com.isolutions.taxi5api.rest.Services.TaxiApiService;
import retrofit.Callback;
import retrofit.RetrofitError;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by George Alekhovitch on 10.01.2015.
 */
public class TaxiDataProvider {

    static final public String TAXI_5_PREFERENCES = "taxe_5_prefernces";
    private String ORDER_ID = "order_id";

    private TaxiApiService taxiApiService;
    private StateChangeListener stateChangeListener;
    private StateChecker stateChecker;
    private RestClient restClient;
    private Integer orderId;
    private Options options;
    private String currentOrderStatus;
    private boolean isReseted;
    private SharedPreferences sharedPreferences;

    public TaxiDataProvider(Context context) {
        sharedPreferences = context.getSharedPreferences(TAXI_5_PREFERENCES, Context.MODE_PRIVATE);
        orderId = sharedPreferences.getInt(ORDER_ID, -1);
        restClient = new RestClient(context);
        taxiApiService = restClient.getTaxiApiService();
    }

    // For adress search
    public ArrayList<Location> getLocations(String destination) {
        return taxiApiService.getLocations(destination);
    }

    public void setStateChangeListener(StateChangeListener stateChangeListener) {
        this.stateChangeListener = stateChangeListener;
    }

    public void cancel() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    taxiApiService.cancel(orderId);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();


    }

    // You should call this after setListener
    public void sendOrder(Order order) {
        isReseted = false;
        cancelOrderRequests();
        stateChecker = new StateChecker();
        stateChecker.execute(order);
    }

    // You should use it when reseting connection to existed order (see getOrder())
    public void resetConnection(Order order) {
        isReseted = true;
        orderId = order.id;
        if (order.options.getClass().equals(Options.class)) {
            options = (Options) order.options;
        }
        currentOrderStatus = ""; // так и должно быть!
        stateChecker = new StateChecker();
        stateChecker.execute(order);
        stateChangeListener.OnStateChange(order);
    }

    // You should use it in onDestroy()
    public void cancelOrderRequests() {
        if (stateChecker != null) {
            stateChecker.cancel(true);
        }
    }

    // You should use it only in preloader. its check the status of last order and if its still
    // in returns the order obbject, that you should use for in method resetConnection(Order order)
    // in activity/fragment that implements listener
    public Order getOrder() {
        if (orderId == -1) {
            return null;
        } else {
            Order order = null;
            try {
                order = taxiApiService.getOrderStatus(orderId);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            if (order.status.equals(State.STATUS_ORDER_CLOSED.toString())
                    || order.status.equals(State.STATUS_CANCELED.toString())
                    || order.status.equals(State.STATUS_CLIENT_APPROVE_TIMEOUT.toString())
                    || order.status.equals(State.STATUS_CLIENT_REJECT.toString())
                    || order.status.equals(State.STATUS_CAR_NOT_FOUNDED.toString())) {
                return null;
            }
            return taxiApiService.getOrderStatus(orderId);
        }
    }

    private Order getOrderStatus() {
        return taxiApiService.getOrderStatus(orderId);
    }

    //when status STATUS_CAR_FOUND you should approve order with this
    public void approveOrder() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (options != null) {
                        taxiApiService.approveOrder(orderId, options);
                    } else {
                        taxiApiService.approveOrder(orderId, new Options());
                    }
                } catch (Exception e) {

                }
            }
        }).start();
    }

    public void leaveFeedback(Feedback feedback, Callback<Feedback> cb) {
        taxiApiService.leaveFeedback(feedback.name, feedback.phone, feedback.text, cb);
    }


    private class StateChecker extends AsyncTask<Order, Order, Void> {

        @Override
        protected Void doInBackground(Order... params) {

            try {
                if (!isReseted) {
                    Order order = taxiApiService.makeOrder(params[0]);
                    sharedPreferences.edit().putInt(ORDER_ID, order.id).commit();
                    currentOrderStatus = order.status;
                    orderId = order.id;
                    if (order.options.getClass().equals(Options.class)) {
                        options = (Options) order.options;
                    }

                    publishProgress(order);
                }
            } catch (RetrofitError e) {
                Order order = new Order();
                order.status = State.STATUS_NO_INTERNET_CONNECTION.toString();
                publishProgress(order);
            } catch (Exception e) {
                Log.d("debug", e.getClass().toString());
            }

            while (true) {
                try {
                    Order order = getOrderStatus();
                    if (order.status.equals(State.STATUS_CLIENT_APPROVE_TIMEOUT.toString())
                            || order.status.equals(State.STATUS_CLIENT_REJECT.toString())
                            || order.status.equals(State.STATUS_CAR_NOT_FOUNDED.toString())
                            || order.status.equals(State.STATUS_CANCELED.toString())) {
                        publishProgress(order);
                        return null;
                    }
                    if (!order.status.equals(currentOrderStatus) || order.status.equals(State.STATUS_CAR_DELIVERING.toString())) {
                        publishProgress(order);
                        currentOrderStatus = order.status;
                    }

                } catch (RetrofitError e) {
                    Order order = new Order();
                    currentOrderStatus = State.STATUS_NO_INTERNET_CONNECTION.toString();
                    order.status = State.STATUS_NO_INTERNET_CONNECTION.toString();
                    publishProgress(order);
                }
                try {
                    TimeUnit.SECONDS.sleep(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (isCancelled()) {
                    return null;
                }
            }
        }

        @Override
        protected void onProgressUpdate(Order... values) {
            super.onProgressUpdate(values);
            stateChangeListener.OnStateChange(values[0]);
        }


    }

}
