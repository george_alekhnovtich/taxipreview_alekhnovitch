package com.isolutions.taxi5api.rest;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.isolutions.taxi5api.rest.Services.TaxiApiService;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 * Created by George Alekhovitch on 10.01.2015.
 */
class RestClient {
    private String baseUrl = "http://taxi5.by/";
    //private String baseUrl = "";
    private static final String USER_ID = "user_id_cache";
    private TaxiApiService apiService;
    private String userId;
    private SharedPreferences sharedPreferences;

    public RestClient(Context context) {

        sharedPreferences = context.getSharedPreferences(TaxiDataProvider.TAXI_5_PREFERENCES, Context.MODE_PRIVATE);
        userId = sharedPreferences.getString(USER_ID, "");
        //baseUrl = context.getResources().getString(R.string.base_url);
        MyCookieManager myCookieManager = new MyCookieManager();
        CookieHandler.setDefault(myCookieManager);

        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(new ItemTypeAdapterFactory())
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade requestFacade) {
                        if (userId != null)
                            requestFacade.addHeader("Cookie", userId);
                    }
                })
                .setEndpoint(baseUrl)
                .setConverter(new GsonConverter(gson))
                .build();
        apiService = restAdapter.create(TaxiApiService.class);
    }

    public TaxiApiService getTaxiApiService() {
        return apiService;
    }

    public void cookie() {
        Log.d("debug", userId == null ? "userid=null" : userId);
    }

    class MyCookieManager extends CookieManager {

        @Override
        public void put(URI uri, Map<String, List<String>> stringListMap) throws IOException {
            super.put(uri, stringListMap);
            if (stringListMap != null && stringListMap.get("Set-Cookie") != null)
                for (String string : stringListMap.get("Set-Cookie")) {
                    Log.d("debug", "COOCKE CATCHED1  " + string);
                    userId = string;
                    sharedPreferences.edit().putString(USER_ID, string).commit();
                }
        }
    }
}
