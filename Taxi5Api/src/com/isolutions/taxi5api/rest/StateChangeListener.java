package com.isolutions.taxi5api.rest;

import com.isolutions.taxi5api.Models.Order;

/**
 * Created by George Alekhovitch on 10.01.2015.
 */
public interface StateChangeListener {
    //this will get states from DataProver
    public void OnStateChange(Order order);
}
