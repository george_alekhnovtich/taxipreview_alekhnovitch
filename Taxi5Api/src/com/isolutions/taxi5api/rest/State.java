package com.isolutions.taxi5api.rest;

/**
 * Created by George Alekhovitch on 12.01.2015.
 */
// All possible states
public enum State {

    STATUS_CAR_SEARCH("car_search"),
    STATUS_CAR_FOUND("car_found"),
    STATUS_CLIENT_APPROVE_TIMEOUT("client_approve_timeout"),
    STATUS_CAR_APPROVED("car_approved"),
    STATUS_CAR_DELIVERING("car_delivering"),
    STATUS_CAR_DELIVERED("car_delivered"),
    STATUS_ORDER_POSTPONED("order_postponed"),
    STATUS_ORDER_IN_PROGRESS("order_in_progress"),
    STATUS_ORDER_COMPLETED("order_completed"),
    STATUS_ORDER_PAID("order_paid"),
    STATUS_ORDER_CLOSED("order_closed"),
    STATUS_ORDER_NOT_PAID("order_not_paid"),
    STATUS_CANCELED("canceled"),
    STATUS_FORCE_CANCELED("force_canceled"),
    STATUS_CLIENT_REJECT("client_approve_reject"),
    STATUS_CAR_NOT_FOUNDED("car_not_founded"),
    STATUS_NO_INTERNET_CONNECTION("no_internet_connection"),
    STATUS_INITIAL("null"),
    STATUS_CLIENT_NOT_FOUND("client_not_found"),
    STATUS_REGISTERED("registered");

    private final String state;

    State(String state) {
        this.state = state;
    }

    public static State fromString(final String state) {
        for (State s : values()) {
            if (s.toString().equals(state)) {
                return s;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return state;
    }
}
