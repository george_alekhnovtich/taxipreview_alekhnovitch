package com.isolutions.taxi5api.Models;

import java.io.Serializable;

/**
 * Created by George Alekhovitch on 05.02.2015.
 */
public class Feedback implements Serializable{
    public String name;
    public String phone;
    public String text;

    public Feedback(String name, String phone, String text) {
        this.name = name;
        this.phone = phone;
        this.text = text;
    }
}
