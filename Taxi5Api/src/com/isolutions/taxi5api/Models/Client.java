package com.isolutions.taxi5api.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by George Alekhovitch on 12.01.2015.
 */
public class Client implements Serializable{

    public Integer id;
    public String name;
    @SerializedName("is_confirmed")
    public Boolean isConfirmed;
    public String phone;
    public Object orders;

}
