package com.isolutions.taxi5api.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by George Alekhovitch on 09.01.2015.
 */
public class Order implements Serializable {
    public Integer id;
    public Eta eta;
    public List<Route> route = new ArrayList<Route>();
    public String amount;
    @SerializedName("is_postponed")
    public Boolean isPostponed;
    @SerializedName("start_at")
    public String startAt;
    @SerializedName("updated_at")
    public String updatedAt;
    @SerializedName("created_at")
    public String createdAt;
    public Client client;
    public String owner ="android";
    //public String comment;
    @SerializedName("car_id")
    public String carId;
    public Object options;
    public String status;
    @SerializedName("status_history")
    public List<String> statusHistory;
    @SerializedName("vehicle_meta")
    public Object car;


}
