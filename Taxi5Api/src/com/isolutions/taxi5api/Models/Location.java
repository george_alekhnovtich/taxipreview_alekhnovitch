package com.isolutions.taxi5api.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by George Alekhovitch on 12.01.2015.
 */
public class Location implements Serializable {
    public Integer id;
    public String name;
    public String street;
    @SerializedName("building")
    public String house = "";
    public String section = "";
    public String city;
    public String locality;
    public String code;
    @SerializedName("discr")
    public String description;
    public String porch;
    public String comment;


}
