package com.isolutions.taxi5api.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by George Alekhovitch on 09.01.2015.
 */
public class Destination implements Serializable {
    public int id;
    public String name;
    public String street;
    @SerializedName("building")
    public String house = "";
    public String housing = "";
    public String city;
    public String code;

    public Destination(int id, String name, String street, String house, String housing, String city, String code) {
        this.id = id;
        this.name = name;
        this.street = street;
        this.house = house;
        this.housing = housing;
        this.city = city;
        this.code = code;
    }

    @Override
    public String toString() {
        return "Destination{" +
                "name='" + name + '\'' +
                '}';
    }
}
