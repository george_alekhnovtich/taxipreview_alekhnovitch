package com.isolutions.taxi5api.Models;

import java.io.Serializable;

/**
 * Created by George Alekhovitch on 14.01.2015.
 */
public class Eta implements Serializable {
    public long seconds;
    public int invert;
    public long minutes;
    public long hours;

    public long getEtaMilliseconds() {
        return hours * 3600000 + minutes * 60000 + seconds * 1000;
    }
}
