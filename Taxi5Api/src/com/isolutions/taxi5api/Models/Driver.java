package com.isolutions.taxi5api.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by George Alekhovitch on 14.01.2015.
 */
public class Driver implements Serializable {
    @SerializedName("firstname")
    public String firstName;
    @SerializedName("middlename")
    public String middleName;
}
