package com.isolutions.taxi5.Utils;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.isolutions.taxi5.R;
import com.isolutions.taxi5api.Models.Location;

import java.util.List;

/**
 * Created by George Alekhovitch on 09.01.2015.
 */
public class DestinationListAdapter extends ArrayAdapter<Location> {
    Context mContext;
    int layoutResourceId;
    List<Location> data;
    String searchElement = "";

    public DestinationListAdapter(Context context, int resource, List<Location> objects) {
        super(context, resource, objects);
        mContext = context;
        layoutResourceId = resource;
        data = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
            Scaler.scaleContents(convertView);
        }

        String name = "";
        String description = "";
        Location location = data.get(position);
        if (location.name == null) {
            name = location.street == null ? "" : location.street;
            description = location.city;
        } else {
            name = location.name;
            description = location.city == null ? "" : location.city + ", " + location.street == null ? "" : location.street + (location.house == null ? "" : ", " + location.house);
        }
        if (!searchElement.equals("")) {
            name = name.replaceAll(searchElement,
                    "<font color='#30aab3'>" + "$0" + "</font>");
        }

        ((TextView) convertView.findViewById(R.id.name)).setText(Html.fromHtml(name));
        ((TextView) convertView.findViewById(R.id.description)).setText(description);
        return convertView;
    }

    public void setSearchElement(String searchElement) {
        this.searchElement = searchElement;
    }
}
