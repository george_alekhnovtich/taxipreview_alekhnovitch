package com.isolutions.taxi5.Utils;

import android.content.Context;
import android.graphics.Point;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

/**
 * Created by George Alekhovitch on 09.01.2015.
 */
public class Scaler {

    private static final int APP_HEIGHT = 1776;
    private static final int APP_WIDTH = 1080;

    private static int appWidth;
    private static int appHeight;

    public static void init(int width, int height){
        appWidth = width;
        appHeight = height;
    }

    public static void scaleContents(View rootView){
        scaleContents(rootView, appWidth, appHeight);
    }

    public static void scaleContents(View rootView, int width, int height) {
        // Compute the scaling ratio

        float xScale = (float) width / APP_WIDTH;
        float yScale = (float) height / APP_HEIGHT;
        float scale = Math.min(xScale, yScale);

        scaleViewAndChildren(rootView, scale);
    }

    private static void scaleViewAndChildren(View root, float scale) {

        // Log.d("scale", root.getClass().toString());
        // Retrieve the view's layout information
        ViewGroup.LayoutParams layoutParams = root.getLayoutParams();
        // Scale the view itself

        if (layoutParams.width != ViewGroup.LayoutParams.MATCH_PARENT
                && layoutParams.width != ViewGroup.LayoutParams.WRAP_CONTENT) {
            layoutParams.width *= scale;
        }
        if (layoutParams.height != ViewGroup.LayoutParams.MATCH_PARENT
                && layoutParams.height != ViewGroup.LayoutParams.WRAP_CONTENT) {
            layoutParams.height *= scale;
        }

        // If this view has margins, scale those too
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) layoutParams;
            marginParams.leftMargin *= scale;
            marginParams.rightMargin *= scale;
            marginParams.topMargin *= scale;
            marginParams.bottomMargin *= scale;
        }

        // Set the layout information back into the view
        root.setLayoutParams(layoutParams);
        // Scale the view's padding
        root.setPadding((int) (root.getPaddingLeft() * scale), (int) (root.getPaddingTop() * scale),
                (int) (root.getPaddingRight() * scale), (int) (root.getPaddingBottom() * scale));

        // If the root view is a TextView, scale the size of its text
        if (root instanceof TextView) {
            TextView textView = (TextView) root;
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textView.getTextSize() * scale);
        }

        // If the root view is a ViewGroup, scale all of its children recursively
        if (root instanceof ViewGroup) {
            ViewGroup groupView = (ViewGroup) root;
            for (int cnt = 0; cnt < groupView.getChildCount(); ++cnt)
                scaleViewAndChildren(groupView.getChildAt(cnt), scale);
        }
    }

    public static int getWidth(Context mContext) {
        int width = 0;
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        return width;
    }

    public static int getHeight(Context mContext) {
        int height = 0;
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        height = size.y;
        return height;
    }
}
