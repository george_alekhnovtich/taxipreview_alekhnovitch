package com.isolutions.taxi5.Utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import com.isolutions.taxi5.R;

/**
 * Created by Dmitriy Zheltko on 09.01.2015.
 */
public class Utils {

    public static void call(Context context) {

        boolean hasTelephony = context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
        if (hasTelephony) {
            Intent intent = new Intent(Intent.ACTION_CALL);

            intent.setData(Uri.parse("tel:" + context.getResources().getString(R.string.taxi_telephone)));
            context.startActivity(intent);
        }

    }

    public static void toast(Context context, String text) {
        Toast.makeText(context, text == null ? "Log" : text, Toast.LENGTH_LONG).show();
    }

    public static void log(String message) {
        Log.d("debug", message);
    }

    public static void hideKeyboard(View v, Context context) {
        InputMethodManager keyboard = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        keyboard.hideSoftInputFromWindow(v.getWindowToken(), 0);

    }
}
