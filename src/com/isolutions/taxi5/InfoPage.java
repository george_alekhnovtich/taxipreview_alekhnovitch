package com.isolutions.taxi5;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.isolutions.taxi5.Utils.Scaler;
import com.isolutions.taxi5.Utils.Utils;

/**
 * Created by George Alekhovitch on 10.01.2015.
 */
public class InfoPage extends Fragment {

    View rootView;
    FeedbackDialog feedbackDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(com.isolutions.taxi5.R.layout.fragment_info, container, false);
        ButterKnife.inject(this, rootView);
        Scaler.scaleContents(rootView);
        feedbackDialog = new FeedbackDialog();
        return rootView;
    }

    @OnClick(R.id.call_button)
    public void call() {
        Utils.call(getActivity());
    }

    @OnClick(R.id.feedback)
    public void recall() {
        feedbackDialog.show(getFragmentManager(), "feedback");
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setInfoVisible(false);
    }

}
